package com.pajato.tks.tv.core

import com.pajato.tks.common.core.TvKey

public interface TvRepo {
    public suspend fun getTv(key: TvKey): Tv
}
