package com.pajato.tks.tv.core

import com.pajato.test.ReportingTestProfiler
import com.pajato.tks.common.core.Company
import com.pajato.tks.common.core.Country
import com.pajato.tks.common.core.Creator
import com.pajato.tks.common.core.Genre
import com.pajato.tks.common.core.SpokenLanguage
import com.pajato.tks.common.core.jsonFormat
import com.pajato.tks.episode.core.Episode
import com.pajato.tks.network.core.Network
import com.pajato.tks.season.core.Season
import kotlinx.serialization.encodeToString
import kotlin.test.Test
import kotlin.test.assertEquals

class TvUnitTest : ReportingTestProfiler() {
    @Test fun `When a default tv object is serialized and deserialized, verify`() {
        val tv = Tv()
        val json = jsonFormat.encodeToString(tv)
        assertEquals("{}", json)
        assertEquals(-1, jsonFormat.decodeFromString<Season>(json).id)
    }

    @Test fun `When a non-default tv object is serialized and deserialized, verify`() {
        val adult = true
        val backdropPath = "/xyzzy.jpg"
        val createdBy: List<Creator> = listOf()
        val episodeRunTime = listOf(50)
        val firstAirDate = "2007-03-25"
        val genres: List<Genre> = listOf(Genre())
        val homepage = "https://torchwood.com"
        val id = 12
        val imdbId = "12Xz7934!"
        val inProduction = false
        val languages = listOf("en-US")
        val lastAirDate = "2007-03-27"
        val lastEpisodeToAir = Episode()
        val name = "xyzzy"
        val nextEpisodeToAir = Episode()
        val networks = listOf(Network())
        val numberOfEpisodes = 23
        val numberOfSeasons = 4
        val originCountry = listOf("US")
        val originalName = "tbd"
        val originalLanguage = "Latin"
        val overview = "A really good story."
        val popularity = 60.0
        val posterPath = "/xyzzy.jpg"
        val productionCompanies = listOf(Company())
        val productionCountries = listOf(Country())
        val seasons = listOf(Season())
        val spokenLanguages = listOf(SpokenLanguage())
        val status = "Finished"
        val tagline = "Concise Quip"
        val type = "Scripted"
        val voteAverage = 44.7
        val voteCount = 6
        val tv =
            Tv(
                adult, backdropPath, createdBy, episodeRunTime, firstAirDate, genres, homepage, id, imdbId, inProduction,
                languages, lastAirDate, lastEpisodeToAir, name, nextEpisodeToAir, networks, numberOfEpisodes,
                numberOfSeasons, originCountry, originalName, originalLanguage, overview, popularity, posterPath,
                productionCompanies, productionCountries, seasons, spokenLanguages, status, tagline, type, voteAverage,
                voteCount,
            )
        val json = jsonFormat.encodeToString(tv)
        val expected =
            """{"adult":true,"backdrop_path":"/xyzzy.jpg","episode_run_time":[50],""" +
                """"first_air_date":"2007-03-25","genres":[{}],"homepage":"https://torchwood.com","id":12,""" +
                """"imdb_id":"12Xz7934!","languages":["en-US"],"last_air_date":"2007-03-27","name":"xyzzy",""" +
                """"networks":[{}],"number_of_episodes":23,"number_of_seasons":4,"origin_country":["US"],""" +
                """"original_name":"tbd","original_language":"Latin","overview":"A really good story.",""" +
                """"popularity":60.0,"poster_path":"/xyzzy.jpg","production_companies":[{}],""" +
                """"production_countries":[{}],"seasons":[{}],"spoken_languages":[{}],"status":"Finished",""" +
                """"tagline":"Concise Quip","type":"Scripted","vote_average":44.7,"vote_count":6}"""
        assertEquals(expected, json)
        assertEquals(12, jsonFormat.decodeFromString<Tv>(json).id)
    }
}
