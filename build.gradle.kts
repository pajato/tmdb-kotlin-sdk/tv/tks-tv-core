plugins {
    alias(libs.plugins.kmp.lib)
}

group = "com.pajato.tks"
version = "0.10.5"
description = "The TMDb Kotlin Sdk (tks) tv feature, core layer, KMP common target project"

kotlin.sourceSets {
    val commonMain by getting {
        dependencies {
            implementation(libs.kotlinx.serialization.json)
            implementation(libs.tks.common.core)
            implementation(libs.tks.episode.core)
            implementation(libs.tks.season.core)
            implementation(libs.tks.network.core)
        }
    }

    val commonTest by getting {
        dependencies {
            implementation(libs.kotlin.test)
            implementation(libs.pajato.test)
        }
    }
}
