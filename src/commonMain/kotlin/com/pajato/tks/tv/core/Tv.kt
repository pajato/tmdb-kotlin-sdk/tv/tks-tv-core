package com.pajato.tks.tv.core

import com.pajato.tks.common.core.BACKDROP_PATH
import com.pajato.tks.common.core.Company
import com.pajato.tks.common.core.Country
import com.pajato.tks.common.core.Creator
import com.pajato.tks.common.core.Genre
import com.pajato.tks.common.core.POSTER_PATH
import com.pajato.tks.common.core.SpokenLanguage
import com.pajato.tks.common.core.TmdbId
import com.pajato.tks.episode.core.Episode
import com.pajato.tks.network.core.Network
import com.pajato.tks.season.core.Season
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable public data class Tv(
    val adult: Boolean = false,
    @SerialName(BACKDROP_PATH)
    val backdropPath: String = "",
    @SerialName("created_by")
    val createdBy: List<Creator> = listOf(),
    @SerialName("episode_run_time")
    val episodeRunTime: List<Int> = listOf(),
    @SerialName("first_air_date")
    val firstAirDate: String = "",
    val genres: List<Genre> = listOf(),
    val homepage: String = "",
    val id: TmdbId = 0,
    @SerialName("imdb_id")
    val imdbId: String? = "",
    @SerialName("in_production")
    val inProduction: Boolean = false,
    val languages: List<String> = listOf(),
    @SerialName("last_air_date")
    val lastAirDate: String = "",
    @SerialName("last_episode_to_air")
    val lastEpisodeToAir: Episode = Episode(),
    val name: String = "",
    @SerialName("next_episode_to_air")
    val nextEpisodeToAir: Episode = Episode(),
    val networks: List<Network> = listOf(),
    @SerialName("number_of_episodes")
    val numberOfEpisodes: Int = 0,
    @SerialName("number_of_seasons")
    val numberOfSeasons: Int = 0,
    @SerialName("origin_country")
    val originCountry: List<String> = listOf(),
    @SerialName("original_name")
    val originalName: String = "",
    @SerialName("original_language")
    val originalLanguage: String = "",
    val overview: String = "",
    val popularity: Double = 0.0,
    @SerialName(POSTER_PATH)
    val posterPath: String = "",
    @SerialName("production_companies")
    val productionCompanies: List<Company> = listOf(),
    @SerialName("production_countries")
    val productionCountries: List<Country> = listOf(),
    val seasons: List<Season> = listOf(),
    @SerialName("spoken_languages")
    val spokenLanguages: List<SpokenLanguage> = listOf(),
    val status: String = "",
    val tagline: String = "",
    val type: String = "",
    @SerialName("vote_average")
    val voteAverage: Double = 0.0,
    @SerialName("vote_count")
    val voteCount: Int = 0,
)
